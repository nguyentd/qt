#include <QCoreApplication>
#include <QTextStream>
#include <QDate>
#include <QTime>
int main(int argc, char *argv[])
{
    //QCoreApplication a(argc, argv);

    //return a.exec();
    QTextStream out(stdout);
    QDate dt = QDate::currentDate();
    QTime qt = QTime::currentTime();
    out << "Date is: " << dt.toString() << endl;
    out << "Time is: " << qt.toString("hh:mm:ss.zzzz") <<endl;
    return 0;

}
